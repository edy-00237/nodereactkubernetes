#!/bin/bash

# Set variables
$source_directory=/home/cloud_user/testdocker/nodereactkubernetes
$destination_directory=/home/cloud_user/testdocker/nodereactkubernetes

# Synchronize files using rsync
rsync -avz --delete --exclude=.git --exclude=node_modules --rsh="ssh" $source_directory/ clouduser@172.31.30.86:$destination_directory/
